# top_flutter_repositories


## Prodject Details : 

1. **Using clean code architecture**  <br /> 
2. **Dependency_injection** <br />
3. **Bloc Statemanagement** <br />
4. **Hydrated_bloc**  <br />
5. **Image caching**   <br />
6. **Data caching** <br />


## Task Info : 
 1. Pagination  [**Done**]
 2. offline mode [**Done**]
 3. Search by name [**Done**]
 4. Sorting [update date-time (**Done**)]
 5. Repositories list show [**Done**]
 6. Repositories Details All [**Done**]
 7. Date formet [**Done**]
 8. repository details offline [**Done**]
 9. Refreshed Api call 30 minit [**on going** ]
 10. filter by star [**not Done**]


## Package List :
  cupertino_icons: ^1.0.2 <br />
  google_fonts: ^6.1.0<br />
  flutter_screenutil: ^5.8.4<br />
  flutter_svg: ^1.0.1<br />
  intl: ^0.18.0<br />
  dartz: ^0.10.1<br />
  dio: ^5.4.0<br />
  dio_cache_interceptor: ^3.5.0<br />
  dio_smart_retry: ^6.0.0<br />
  equatable: ^2.0.5<br />
  get_it: ^7.6.7<br />
  hydrated_bloc: ^9.1.3<br />
  path_provider: ^2.1.2<br />
  cached_network_image: ^3.2.3 <br />
  
