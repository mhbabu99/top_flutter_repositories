import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:path_provider/path_provider.dart';
import 'package:top_flutter_repositories/core/di/dependency_injector.dart';

import 'package:top_flutter_repositories/core/router/on_generate_route.dart';
import 'package:top_flutter_repositories/core/router/theme/app_theme.dart';
import 'package:top_flutter_repositories/features/homepage/presentation/cubit/repositories_cubit/repositories_cubit.dart';

void main() async {
  final RouteGenerator routeGenerator = RouteGenerator();
  WidgetsFlutterBinding.ensureInitialized();


  // --------------Dependencies Injector ----------------------------
   await configureDependencies();
  // --------------Dependencies Injector end-------------------------

  // --------------Hydrated Bloc----------------------------
  HydratedBloc.storage = await HydratedStorage.build(
    storageDirectory: await getApplicationDocumentsDirectory(),
  );
  // --------------Hydrated Bloc end------------------------

  // --------------Error handling---------------------------
  FlutterError.onError = (FlutterErrorDetails details) {
    FlutterError.dumpErrorToConsole(details);
  };
  // --------------Error handling end-----------------------

  runApp(MyApp(
    routeGenerator: routeGenerator,
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({
    super.key,
    required this.routeGenerator,
  });
  final RouteGenerator routeGenerator;
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(375, 812),
      minTextAdapt: true,
      splitScreenMode: true,
      builder: (context, child) {
        return MaterialAppWidget(routeGenerator: routeGenerator);
      },
    );
  }
}

class MaterialAppWidget extends StatelessWidget {
  const MaterialAppWidget({
    Key? key,
    required this.routeGenerator,
  }) : super(key: key);

  final RouteGenerator routeGenerator;

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return MultiBlocProvider(
      providers: [
        BlocProvider<RepositoriesCubit>(
          create: (context) => sl<RepositoriesCubit>(),

          //   BlocProvider<RepositoriesBloc>(
          // create: (context) => sl<RepositoriesBloc>(),
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        themeMode: ThemeMode.system,
        theme: AppTheme.lightTheme(textTheme),
        darkTheme: AppTheme.darkTheme(textTheme),
        onGenerateRoute: RouteGenerator().onGenerateRoute,
      ),
    );
  }
}
