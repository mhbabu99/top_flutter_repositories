class AppAssets {
  static const String imageAssetsPath = "assets/images";
  static const String icons = 'assets/icons';
  static const String lotties = 'assets/lotties';

  //Icons :

  static const String filter = "$icons/filter.svg";
  static const String search = "$icons/search.svg";

  static const String clearIcon = "$icons/Clear_icon.svg";
}
