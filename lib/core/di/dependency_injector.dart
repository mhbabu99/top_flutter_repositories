import 'package:get_it/get_it.dart';
import 'package:top_flutter_repositories/features/homepage/data/datasources/repositories_remote.dart';
import 'package:top_flutter_repositories/features/homepage/data/repositories/repositories_repository_impl.dart';
import 'package:top_flutter_repositories/features/homepage/domain/repositories/repositories_repository.dart';
import 'package:top_flutter_repositories/features/homepage/domain/usecases/repositories.dart';
import 'package:top_flutter_repositories/features/homepage/presentation/cubit/repositories_cubit/repositories_cubit.dart';

final sl = GetIt.instance;

Future<void> configureDependencies() async {
  await _repositories();
}

//Repositories : Add your dependencies here
Future<void> _repositories() async {
  // Repositories cubit
  sl.registerFactory<RepositoriesCubit>(() => RepositoriesCubit(repositoriesUseCase: sl()));
  // Repositories use-case
  sl.registerLazySingleton(() => RepositoriesUseCase( repositoriesRepository: sl(),));
  // Repositories : Add your repositories here
  sl.registerLazySingleton<RepositoriesRepository>(
      () => RepositoriesRepositoryImpl(repositoriesRemoteServices: sl()));
  // Repositories : Add your data sources here
  sl.registerLazySingleton(() => RepositoriesRemoteServices());
}
