import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:top_flutter_repositories/config/enum/bloc_api_state.dart';
import 'package:top_flutter_repositories/core/utils/app_assets.dart';
import 'package:top_flutter_repositories/features/homepage/domain/entities/repositories_entities.dart';
import 'package:top_flutter_repositories/features/homepage/presentation/cubit/repositories_cubit/repositories_cubit.dart';
import 'package:top_flutter_repositories/features/homepage/presentation/widget/custom_dropdown_widget.dart';
import 'package:top_flutter_repositories/features/homepage/presentation/widget/input_field_widget.dart';

class FilterPage extends StatefulWidget {
  const FilterPage({super.key});

  @override
  State<FilterPage> createState() => _FilterPageState();
}

class _FilterPageState extends State<FilterPage> {
  late TextEditingController fullName;

  @override
  void initState() {
    super.initState();
    fullName = TextEditingController(text: '')..addListener(() {
        setState(() {});
      });
        dob = ValueNotifier('');
  }


   late final ValueNotifier<String> dob;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Text(
          "Filter",
          style: TextStyle(
              color: Colors.black,
              fontSize: 18.sp,
              fontWeight: FontWeight.w600),
        ),
      ),
      body: Padding(
        padding:  EdgeInsets.all(15.w),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [

          10.verticalSpace,
           Text(
             "Full name",
             style: TextStyle(color: Colors.black, fontSize: 15.sp,fontWeight: FontWeight.w600),
           ),
           8.verticalSpace,
          InputFieldWidget(
            logo: AppAssets.search,
            suffixlogo: AppAssets.clearIcon,
            lable: 'search ',
            inputController: fullName,
            sufixIconview: fullName.text.isNotEmpty? true:false,
            onTap: (){
              setState(() {
                fullName.text ='';
              });
            },
            passwordVisible: false,
            prefixIconview: fullName.text.isNotEmpty? false:true,
          ),

          20.verticalSpace,
          SizedBox(
            height: 60.h,
            width: double.infinity,
            child: ElevatedButton(
               style: ElevatedButton.styleFrom(
                backgroundColor: Colors.blue[300],
  shape: RoundedRectangleBorder(
    borderRadius: BorderRadius.circular(5.0),
  ),
               ),
              child: Text("Search",style: TextStyle(color: Colors.white,fontWeight: FontWeight.w500,fontSize: 16.sp),),
               onPressed: () { 
context.read<RepositoriesCubit>().name = fullName.text;
    context.read<RepositoriesCubit>().getRepositoriess(
        repositoriesEntities: RepositoriesEntities(
            totalCount: 0, incompleteResults: false, items: const []),
        checkState: NormalApiState.initial,
       );
       Navigator.pop(context);

                },)),
        ]),
      ),
    );
  }

    Future<DateTime?> show(
      {required BuildContext context,
      required DateTime initialDate,
      required DateTime lastDate,
      required String helpText,
      required DateTime firstDate,
      required bool isReturn}) async {
    return await showDatePicker(
      context: context,
      helpText: helpText,
      initialDate: initialDate,
      firstDate: firstDate,
      lastDate: lastDate,
      builder: (BuildContext context, Widget? child) {
        return Theme(
          data: ThemeData.light().copyWith(
            canvasColor: Colors.red,
            colorScheme: const ColorScheme.light(
              primary:Colors.blue,
              secondary: Colors.red,
            ),
            // Change the text theme
            textTheme: const TextTheme(
              bodyLarge: TextStyle(color: Colors.blue),
              bodyMedium: TextStyle(color: Colors.black),
              titleLarge: TextStyle(color: Colors.pink),
              titleMedium: TextStyle(
                color: Colors.black,
              ),
            ),
          ),
          child: child!,
        );
      },
    ).then((selectedDate) {
      if (selectedDate != null) {
        return selectedDate;
      }
      return null;
    }).onError((error, stackTrace) => null);
  }
}
