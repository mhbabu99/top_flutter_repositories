import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:top_flutter_repositories/features/homepage/data/models/repositories_model.dart';
import 'package:top_flutter_repositories/features/homepage/presentation/widget/on_network_image.dart';

class RepositoriesDetailsPage extends StatelessWidget {
  const RepositoriesDetailsPage({super.key,required this.item});
  final Item  item ;

  @override
  Widget build(BuildContext context) {
var dateTime ;
  if (item.updatedAt != '') {
      dateTime = DateTime.parse(item.updatedAt);
    }
    return Scaffold(
      appBar: AppBar(elevation: 0,title: Text("Repositories Details", style: TextStyle(color: Colors.black, fontSize: 18.sp,fontWeight: FontWeight.w600),),),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
    
               Center(
                child: CircleAvatar(
                  radius: 80,
                  child: OnNetWorkImage(
                            url: item.owner.avatarUrl,
                            // width: width,
                            // height: height * .90,
                            fit: BoxFit.fill,
                            errorWidget: Container(
                              alignment: Alignment.center,
                              color: Colors.grey.shade200,
                              child: Icon(Icons.image_not_supported),
                            ),
                          ),
                ),
              ),
              20.verticalSpace,
              Align(
                  alignment: Alignment.center,
                  child: Text(
                item.fullName.toString(),
                    style: TextStyle(color: Colors.black, fontSize: 18.sp,fontWeight: FontWeight.w600),
                  )),
              5.verticalSpace,
              Align(
                  alignment: Alignment.center,
                  child: Text(
                     item.id.toString(),
                    style: TextStyle(color: Colors.black, fontSize: 16.sp,fontWeight: FontWeight.w600),
                  )),
              30.verticalSpace,
              Text(
                "About:",
                style: TextStyle(color: Colors.black, fontSize: 16.sp,fontWeight: FontWeight.w600),
              ),
5.verticalSpace,
  Text(
               """${item.description} """,
                style: TextStyle(color: Colors.black, fontSize: 14.sp),
              ),
              20.verticalSpace,
              Text("Date : ${DateFormat('MM-dd-yy : hh:mm').format(dateTime)}",      style: TextStyle(color: Colors.black, fontSize: 16.sp,fontWeight: FontWeight.w600),)
            ]),
      ),
    );
  }
}
