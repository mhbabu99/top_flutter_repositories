import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:top_flutter_repositories/config/enum/bloc_api_state.dart';
import 'package:top_flutter_repositories/core/router/route_name.dart';
import 'package:top_flutter_repositories/features/homepage/domain/entities/repositories_entities.dart';
import 'package:top_flutter_repositories/features/homepage/presentation/cubit/repositories_cubit/repositories_cubit.dart';
import 'package:top_flutter_repositories/features/homepage/presentation/widget/repositories_card.dart';
import 'package:top_flutter_repositories/features/homepage/presentation/widget/title_bar.dart';

class HomePage extends StatefulWidget {
  const HomePage({
    super.key,
  });
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {




   late final ValueNotifier<String> dob;



  @override
  void initState() {
    super.initState();
      dob = ValueNotifier('');
    context.read<RepositoriesCubit>().getRepositoriess(
          repositoriesEntities: RepositoriesEntities(
              totalCount: 0, incompleteResults: false, items: const []),
          checkState: NormalApiState.initial,
        );
    appBarCollapsingListener();
  }




  final _scrollController = ScrollController();
  appBarCollapsingListener() {
    _scrollController.addListener(() {
      log(_scrollController.position.pixels.toString());
      double position = _scrollController.position.pixels;
      double maxPos = _scrollController.position.maxScrollExtent;
      log("Checking Max${_scrollController.position.maxScrollExtent}");
      if (position == maxPos) {
        log('calling api ----------');
        final state = context.read<RepositoriesCubit>().state;
        if (state.apiState == NormalApiState.loaded && state.endpoint != true) {
          context.read<RepositoriesCubit>().getRepositoriess(
                repositoriesEntities: state.repositoriesEntities,
                checkState: NormalApiState.loaded,
              );
        } else {
          ScaffoldMessenger.maybeOf(context)?.showSnackBar(SnackBar(
            content: Text(
              state.errorMessage,
              style: const TextStyle(color: Colors.white),
            ),
            backgroundColor: Colors.red,
          ));
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.only(left: 15.w, right: 15.w),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            60.verticalSpace,
            TitleBar(
              onTap: () {
                Navigator.pushNamed(context, RouteName.filterPage);
              },
            ),
            Expanded(
              child: BlocConsumer<RepositoriesCubit, RepositoriesState>(
                listener: (context, state) {
                  if (state.apiState == NormalApiState.failure &&
                      state.repositoriesEntities.items.isNotEmpty) {
                    // snackbar
                    ScaffoldMessenger.maybeOf(context)?.showSnackBar(SnackBar(
                      content: Text(
                        state.errorMessage,
                        style: const TextStyle(color: Colors.white),
                      ),
                      backgroundColor: Colors.red,
                    ));
                  }
                },
                builder: (context, state) {
                  switch (state.apiState) {
                    case NormalApiState.initial:
                      return Center(
                        child: Text(state.errorMessage,
                            style: const TextStyle(color: Colors.red),
                            textAlign: TextAlign.center),
                      );

                    case NormalApiState.loading:
                      if (state.repositoriesEntities.items.isNotEmpty) {
                        return ListView.builder(
                          physics: const ClampingScrollPhysics(),
                          itemCount: state.repositoriesEntities.items.length,
                          itemBuilder: (context, index) {
                            return Card(
                              child: ListTile(
                                title: Text(
                                  state.repositoriesEntities.items[index].name
                                      .toString(),
                                ),
                              ),
                            );
                          },
                        );
                      }
                      return const Center(child: CircularProgressIndicator());

                    case NormalApiState.loaded:
                      return RefreshIndicator(
                        onRefresh: () async {
                          context.read<RepositoriesCubit>().getRepositoriess(
                                repositoriesEntities: RepositoriesEntities(
                                    totalCount: 0,
                                    incompleteResults: false,
                                    items: const []),
                                checkState: NormalApiState.initial,
                              );
                        },
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            5.verticalSpace,
                            Row(
                              children: [
                                Expanded(
                                  child: Text(
                                    "About  ${state.repositoriesEntities.totalCount} results",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 14.sp,
                                    ),
                                  ),
                                ),
                                //    Expanded(child: SessionWidget(repositoriesEntities: state.repositoriesEntities,))
                              ],
                            ),
                              Container(
                    height: 65.5.h,
                    width: double.infinity,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(width: 1, color: Colors.black12)),
                    alignment: Alignment.center,
                    padding: EdgeInsets.only(left: 2.w),
                    child: ValueListenableBuilder(
                        valueListenable: dob,
                        builder: (context, valueListenableDOB, child) {
                          return InkWell(
                            onTap: () async {
                              DateTime? value;
                              value = await show(
                                context: context,
                                isReturn: true,
                                initialDate: DateTime.now(),
                                firstDate:DateTime(DateTime.now().year - 10),
                                lastDate:  DateTime.now(),
                                helpText: "Age range maximum 5",
                              );
                              if (value != null) {
                                dob.value = value.toIso8601String();
                       // ignore: use_build_context_synchronously
                       context.read<RepositoriesCubit>().filterByUpdateDate(updatedate: dob.value);
                              
                              //  setState(() {});
                              }
                            },
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                1.verticalSpace,
                                Row(
                                  children: [
                                  
                                    5.horizontalSpace,
                                    Expanded(
                                      flex: 15,
                                      child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            3.verticalSpace,
                                            Text(
                                              "Update date",
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 10.sp,
                                                  fontWeight: FontWeight.w200),
                                            ),
                                            2.verticalSpace,
                                            Align(
                                              alignment: Alignment.centerLeft,
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(5.0),
                                                child: Text(
                                                  dob.value.isNotEmpty
                                                      ? DateFormat(
                                                              "MMM dd, yyyy")
                                                          .format(
                                                              DateTime.parse(
                                                                  dob.value))
                                                      : "Update  Time",
                                                  style: TextStyle(
                                                    color:
                                                        const Color(0xff474747),
                                                    fontSize: 12.sp,
                                                    fontFamily: "Poppins",
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ]),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          );
                        }),
                  ),
                            Expanded(
                              child: ListView.builder(
                                physics: const ClampingScrollPhysics(),
                                itemCount:
                                    state.repositoriesEntities.items.length,
                                controller: _scrollController,
                                itemBuilder: (context, index) {
                                  return RepositoriesCard(
                                    item:
                                        state.repositoriesEntities.items[index],
                                    onTap: () {
                                      Navigator.pushNamed(context,
                                          RouteName.repositoriesDetailsPage,
                                          arguments: state.repositoriesEntities
                                              .items[index]);
                                    },
                                  );
                                },
                              ),
                            ),
                          ],
                        ),
                      );
                    case NormalApiState.lazy:
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          5.verticalSpace,
                          Text(
                            "About  ${state.repositoriesEntities.totalCount} results",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 14.sp,
                            ),
                          ),
                          Expanded(
                            child: ListView.builder(
                              physics: const ClampingScrollPhysics(),
                              itemCount:
                                  state.repositoriesEntities.items.length,
                              controller: _scrollController,
                              itemBuilder: (context, index) {
                                return RepositoriesCard(
                                  item: state.repositoriesEntities.items[index],
                                  onTap: () {
                                    Navigator.pushNamed(context,
                                        RouteName.repositoriesDetailsPage,
                                        arguments: state
                                            .repositoriesEntities.items[index]);
                                  },
                                );
                              },
                            ),
                          ),
                        ],
                      );
                    case NormalApiState.failure:
                      if (state.repositoriesEntities.items.isNotEmpty) {
                        return RefreshIndicator(
                            onRefresh: () async {
                              // context.read<RepositoriesCubit>().getRepositoriess(
                              //       repositoriesEntities: RepositoriesEntities(
                              //           totalCount: 0,
                              //           incompleteResults: false,
                              //           items: []),
                              //       cstate: NormalApiState.initial,
                              //     );
                            },
                            child: Column(
                              children: [
                                5.verticalSpace,
                                Text(
                                  "About  ${state.repositoriesEntities.totalCount} results",
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 14.sp,
                                  ),
                                ),
                                Expanded(
                                  child: ListView.builder(
                                    physics: const ClampingScrollPhysics(),
                                    itemCount:
                                        state.repositoriesEntities.items.length,
                                    controller: _scrollController,
                                    itemBuilder: (context, index) {
                                      return RepositoriesCard(
                                        item: state
                                            .repositoriesEntities.items[index],
                                        onTap: () {
                                          Navigator.pushNamed(context,
                                              RouteName.repositoriesDetailsPage,
                                              arguments: state
                                                  .repositoriesEntities
                                                  .items[index]);
                                        },
                                      );
                                    },
                                  ),
                                ),
                              ],
                            ));
                      }
                      return Center(
                        child: Text(state.errorMessage,
                            style: const TextStyle(color: Colors.red),
                            textAlign: TextAlign.center),
                      );

                    default:
                      return Center(
                        child: Text(state.errorMessage,
                            style: const TextStyle(color: Colors.red),
                            textAlign: TextAlign.center),
                      );
                  }
                },
              ),
            )
          ],
        ),
      ),
    );
  }
  Future<DateTime?> show(
      {required BuildContext context,
      required DateTime initialDate,
      required DateTime lastDate,
      required String helpText,
      required DateTime firstDate,
      required bool isReturn}) async {
    return await showDatePicker(
      context: context,
      helpText: helpText,
      initialDate: initialDate,
      firstDate: firstDate,
      lastDate: lastDate,
      builder: (BuildContext context, Widget? child) {
        return Theme(
          data: ThemeData.light().copyWith(
            canvasColor: Colors.red,
            colorScheme: const ColorScheme.light(
              primary:Colors.blue,
              secondary: Colors.red,
            ),
            // Change the text theme
            textTheme: const TextTheme(
              bodyLarge: TextStyle(color: Colors.blue),
              bodyMedium: TextStyle(color: Colors.black),
              titleLarge: TextStyle(color: Colors.pink),
              titleMedium: TextStyle(
                color: Colors.black,
              ),
            ),
          ),
          child: child!,
        );
      },
    ).then((selectedDate) {
      if (selectedDate != null) {
        return selectedDate;
      }
      return null;
    }).onError((error, stackTrace) => null);
  }
}
