import 'dart:developer';
import 'package:equatable/equatable.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:top_flutter_repositories/config/enum/bloc_api_state.dart';
import 'package:top_flutter_repositories/features/homepage/data/models/repositories_model.dart';
import 'package:top_flutter_repositories/features/homepage/domain/entities/repositories_entities.dart';
import 'package:top_flutter_repositories/features/homepage/domain/usecases/repositories.dart';
part 'repositories_state.dart';

class RepositoriesCubit extends HydratedCubit<RepositoriesState> {
  final RepositoriesUseCase repositoriesUseCase;
  int pageNumber = 1;
  String name = '';
  String short = '';
  String star = '';
  bool isTimeCounting = false;
  RepositoriesEntities mainRepositoriesEntities =
      RepositoriesEntities(totalCount: 0, incompleteResults: false, items: const []);

  RepositoriesCubit({required this.repositoriesUseCase})
      : super(
          RepositoriesState(
              apiState: NormalApiState.initial,
              errorMessage: "Please Swipe Down to Refresh",
              endpoint: false,
              repositoriesEntities: RepositoriesModel(
                  totalCount: 0, incompleteResults: false, items: const [])),
        );

  @override
  RepositoriesState? fromJson(Map<String, dynamic> json) {
    try {
      final RepositoriesModel repositoriess = RepositoriesModel.fromJson(json);
      // ignore: invalid_use_of_visible_for_testing_member
      return RepositoriesState(
        repositoriesEntities: repositoriess,
        apiState: NormalApiState.loaded,
        errorMessage: '',
        endpoint: false,
      );
    } catch (_) {
// ignore: invalid_use_of_visible_for_testing_member
      return RepositoriesState(
          repositoriesEntities: RepositoriesModel(
              totalCount: 0, incompleteResults: false, items: const []),
          apiState: NormalApiState.initial,
          endpoint: false,
          errorMessage: '');
    }
  }

  @override
  Map<String, dynamic>? toJson(RepositoriesState state) {
    return {
      "total_count": state.repositoriesEntities.totalCount,
      "incomplete_results": state.repositoriesEntities.incompleteResults,
      "items": List<dynamic>.from(
          state.repositoriesEntities.items.map((x) => x.toJson())),
    };
  }

  Future<void> getRepositoriess({
    required RepositoriesEntities repositoriesEntities,
    required NormalApiState checkState,
  }) async {
    isTimeCounting = true;
    if (checkState == NormalApiState.initial) {
      pageNumber = 1;
      await repositoriesUseCase
          .getRepositories(
              page: pageNumber,
              name: name.isEmpty ? "Flutter" : name,
              star: star,
              short: short)
          .then((repositoriess) {
        pageNumber = pageNumber + 1;
        mainRepositoriesEntities = repositoriess;
        if (repositoriess.items.length >= 10) {
          emit(state.copyWith(
              repositoriesEntities: repositoriess,
              apiState: NormalApiState.loaded,
              errorMessage: "",
              endpoint: false));
        } else {
          emit(state.copyWith(
              repositoriesEntities: repositoriess,
              apiState: NormalApiState.loaded,
              errorMessage: "",
              endpoint: true));
        }
      }).onError((error, stackTrace) {
        emit(
          state.copyWith(
              apiState: NormalApiState.failure, errorMessage: error.toString()),
        );
      });
    } else if (checkState == NormalApiState.loaded) {
      await repositoriesUseCase
          .getRepositories(
              page: pageNumber,
              name: name.isEmpty ? "Flutter" : name,
              star: star,
              short: short)
          .then((repositoriess) {
        log("%%%%%%% ${repositoriesEntities.items.length}");
        emit(state.copyWith(
            repositoriesEntities: repositoriesEntities,
            apiState: NormalApiState.lazy,
            errorMessage: ""));
        List<Item> results = [
          ...repositoriesEntities.items,
          ...repositoriess.items,
        ];
        repositoriesEntities.items = results;
        log("+++++++++ ${repositoriesEntities.items.length}");
        pageNumber = pageNumber + 1;
            mainRepositoriesEntities = repositoriess;
        if (repositoriess.items.isEmpty) {
          emit(state.copyWith(
            repositoriesEntities: repositoriesEntities,
            apiState: NormalApiState.loaded,
            errorMessage: "No more Data ",
            endpoint: true,
          ));
        } else {
          emit(state.copyWith(
              repositoriesEntities: repositoriesEntities,
              apiState: NormalApiState.loaded,
              endpoint: false,
              errorMessage: ""));
        }
      }).onError((error, stackTrace) {
        emit(
          state.copyWith(
              apiState: NormalApiState.failure, errorMessage: error.toString()),
        );
      });
    }
    //   emit(state.copyWith(apiState: NormalApiState.loading));
  }

  void filterByUpdateDate({required String updatedate}) {
    log("filter data : ${updatedate.substring(0, 10)}");
    log("filter length : ${mainRepositoriesEntities.items.length}");
    emit(state.copyWith(
        repositoriesEntities: mainRepositoriesEntities,
        apiState: NormalApiState.lazy,
        endpoint: false,
        errorMessage: ""));
String value = updatedate.substring(0,10).toLowerCase();
if(value =="2024-01-26"){
  log('check ok');
}
    List<Item> searchList = mainRepositoriesEntities.items
        .where((item) =>
            item.updatedAt.toLowerCase().contains(value.toLowerCase()))
        .toList();
    if (updatedate == '') {
      emit(state.copyWith(
          repositoriesEntities: mainRepositoriesEntities,
          apiState: NormalApiState.loaded,
          endpoint: false,
          errorMessage: ""));
    } else {
      emit(state.copyWith(
          repositoriesEntities: RepositoriesEntities(
              totalCount: mainRepositoriesEntities.totalCount,
              incompleteResults: mainRepositoriesEntities.incompleteResults,
              items: searchList),
          apiState: NormalApiState.loaded,
          endpoint: false,
          errorMessage: ""));
    }
  }
}
