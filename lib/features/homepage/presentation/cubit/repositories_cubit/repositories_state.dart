part of 'repositories_cubit.dart';

class RepositoriesState extends Equatable {
  final RepositoriesEntities repositoriesEntities;
  final NormalApiState apiState;
  final String errorMessage;
  final bool endpoint;

  const RepositoriesState(
      {required this.repositoriesEntities,
      required this.apiState,
      required this.errorMessage,required this.endpoint});



  RepositoriesState copyWith({
    RepositoriesEntities? repositoriesEntities,
    NormalApiState? apiState,
    String? errorMessage,
    bool ? endpoint,
  }) {
    return RepositoriesState(
      repositoriesEntities: repositoriesEntities ?? this.repositoriesEntities,
      apiState: apiState ?? this.apiState,
      errorMessage: errorMessage ?? this.errorMessage,
      endpoint: endpoint ?? this.endpoint
    );
  }

    @override
  List<Object?> get props => [repositoriesEntities, apiState, errorMessage];
}

// final class TodoList extends RepositoriesState {
//   const TodoList({
//     required List<RepositoriesEntities> repositoriesEntities,
//     required NormalApiState apiState,
//     required String errorMessage,
//   }) : super(
//           repositoriesEntities: repositoriesEntities,
//           apiState: apiState,
//           errorMessage: errorMessage,
//         );
//   @override
//   List<Object?> get props => [repositoriesEntities, apiState, errorMessage];

//   TodoList copyWith({
//     List<RepositoriesEntities>? repositoriesEntities,
//     NormalApiState? apiState,
//     String? errorMessage,
//   }) {
//     return TodoList(
//       repositoriesEntities: repositoriesEntities ?? this.repositoriesEntities,
//       apiState: apiState ?? this.apiState,
//       errorMessage: errorMessage ?? this.errorMessage,
//     );
//   }
// }
