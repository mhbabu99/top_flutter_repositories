import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';


class InputFieldWidget extends StatelessWidget {
  final String logo;
  final String? suffixlogo;
  final String lable;
  final Function()? onTap;
  final String? Function(String?)? validator;
  final TextEditingController inputController;
  final int? maxLine;
  final bool prefixIconview;
  final bool sufixIconview;
  final bool passwordVisible;


  const InputFieldWidget(
      {Key? key,
      required this.logo,
      this.suffixlogo,
      this.onTap,
      required this.lable,
       this.validator,
      required this.inputController,

      this.maxLine,
      required this.sufixIconview,
      required this.passwordVisible,
      required this.prefixIconview})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      cursorColor: Colors.white,
      controller: inputController,
      validator: validator,
      obscureText: passwordVisible,
      maxLines: maxLine ?? 1,
      style: GoogleFonts.poppins(
        fontSize: 14.sp,
        fontWeight: FontWeight.w400,
        color: Colors.black,
        fontStyle: FontStyle.normal,
        textStyle: Theme.of(context).textTheme.displayLarge,
      ),
      decoration: InputDecoration(
        isDense: true,
        
        suffixIcon: sufixIconview
            ? Padding(
                padding: EdgeInsets.all(15.w),
                child: GestureDetector(
                  onTap: onTap,
                  child: SvgPicture.asset(
                    suffixlogo!,
                    //  color: Colors.white,
                    height: 20.r,
                  ),
                ),
              )
            : null,
        // contentPadding: EdgeInsets.only(left: 10.w),
        prefixIcon: prefixIconview
            ? Padding(
                padding: EdgeInsets.all(15.w),
                child: SvgPicture.asset(
                  logo,
                  //  color: Colors.white,
                  height: 20.r,
                ),
              )
            : null,
        //    labelText: label,
        filled: true, //<-- SEE HERE
        fillColor: Colors.white, //<-- SEE HERE
        hintText: lable,
        labelStyle: GoogleFonts.poppins(
          fontWeight: FontWeight.w400,
          fontSize: 16.sp,
          color: const Color(0xff474747),
          fontStyle: FontStyle.normal,
          textStyle: Theme.of(context).textTheme.displayLarge,
        ),

        hintStyle: GoogleFonts.poppins(
          color: const Color(0xff8E8E93),
          fontWeight: FontWeight.w400,
          fontSize: 14.sp,
          fontStyle: FontStyle.normal,
          textStyle: Theme.of(context).textTheme.displayLarge,
        ),

        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
          borderSide:  BorderSide(width: 1.5, color: Color(0xffEBEBEB)),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
          borderSide:  BorderSide(width: 1.5, color: Color(0xffEBEBEB)),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
          borderSide:
              const BorderSide(width: 1.5, color:Colors.blue),
        ),
        errorStyle: GoogleFonts.poppins(
          color: Colors.redAccent,
          fontSize: 10,
          fontWeight: FontWeight.w400,
          fontStyle: FontStyle.normal,
          textStyle: Theme.of(context).textTheme.displayLarge,
        ),
      ),
    );
  }
}
