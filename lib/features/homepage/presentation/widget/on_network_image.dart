// ignore_for_file: public_member_api_docs, sort_constructors_first
// ignore_for_file: unnecessary_this

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class OnNetWorkImage extends StatelessWidget {
  final String url;
  final double? height;
  final double? width;
  final BoxShape? shape;
  final BoxFit? fit;
  final Widget? errorWidget;
  final Widget? progressIndicatorBuilder;
  final Color? backgroundColor;
  final BlendMode? colorBlendMode;

  const OnNetWorkImage({
    Key? key,
    required this.url,
    this.height,
    this.width,
    this.shape,
    this.fit,
    this.errorWidget,
    this.progressIndicatorBuilder,
    this.backgroundColor,
    this.colorBlendMode,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: url,
      height: height,
      color: backgroundColor,
      width: width,
      placeholderFadeInDuration: const Duration(microseconds: 50),
      maxHeightDiskCache: height?.toInt(),
      maxWidthDiskCache: width?.toInt(),
      memCacheHeight: height?.toInt(),
      memCacheWidth: width?.toInt(),
      colorBlendMode: colorBlendMode,
      progressIndicatorBuilder: (context, url, progress) {
      
        return progressIndicatorBuilder ??
            Container(
              alignment: Alignment.center,
              color: Colors.grey.shade200,
              child: const CupertinoActivityIndicator(),
            );
      },
      imageBuilder: (context, imageProvider) {
        return Container(
          decoration: BoxDecoration(
            shape: shape ?? BoxShape.rectangle,
            image: DecorationImage(
              image: imageProvider,
              fit: fit ?? BoxFit.cover,
            ),
          ),
        );
      },
      errorWidget: (context, url, error) => Padding(
        padding: const EdgeInsets.all(8.0),
        child: errorWidget ??
            Container(
              alignment: Alignment.center,
              color: Colors.grey.shade200,
              child: const CupertinoActivityIndicator(),
            ),
),
    );
  }
}
