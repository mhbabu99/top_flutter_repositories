import 'package:flutter/material.dart';
import 'package:top_flutter_repositories/features/homepage/data/models/repositories_model.dart';

class RepositoriesCard extends StatelessWidget{
  const RepositoriesCard({super.key ,required this.onTap,required this.item});
  final Function() onTap;
 final Item item ;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return  InkWell(
      onTap: onTap,
      child:  Card(
        
                  elevation: 2,
                  child: ListTile(
                      title: Text(item.fullName??'',maxLines: 1,overflow: TextOverflow.ellipsis,),
                      subtitle: Text(item.id.toString()),
                      leading: CircleAvatar(
                      //  backgroundColor: Color(0xff000000),
                        child: Text(item.name!.substring(0, 1).toUpperCase()),
                      ))),
    );
  }
}