
// import 'dart:async';
// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:flutter_screenutil/flutter_screenutil.dart';
// import 'package:google_fonts/google_fonts.dart';
// import 'package:top_flutter_repositories/config/enum/bloc_api_state.dart';
// import 'package:top_flutter_repositories/features/homepage/domain/entities/repositories_entities.dart';
// import 'package:top_flutter_repositories/features/homepage/presentation/cubit/repositories_cubit/repositories_cubit.dart';

// class SessionWidget extends StatefulWidget {
//   const SessionWidget({
//     super.key,
//     required this.repositoriesEntities,
//   });
// final RepositoriesEntities repositoriesEntities;
//   @override
//   State<SessionWidget> createState() => _SessionWidgetState();
// }

// class _SessionWidgetState extends State<SessionWidget> {



//   @override
//   void initState() {
//     super.initState();
//     if(context.read<RepositoriesCubit>().isTimeCounting == true){
//   startTimer(context,1);
//     }else{
//       resetTimer();
//     }
   
//   }
//  static const int TOTAL_SECONDS = 1800; // 10 minutes in seconds
//   int secondsRemaining = TOTAL_SECONDS;
//   bool isTimeCounting = false;
//   Timer? _timer;
//   void startTimer(BuildContext context, int timer) {
//     isTimeCounting = true;
//     secondsRemaining = timer * 60;
//     _timer?.cancel();
//     _timer = Timer.periodic(const Duration(seconds: 1), (timer) {
//       if (secondsRemaining > 0) {
//         secondsRemaining--;
//       } else {
//         isTimeCounting = false;
//        context.read<RepositoriesCubit>().timerActiveFuction(repositoriesEntities: widget.repositoriesEntities,
//         checkState:NormalApiState.loaded ); 
//         _timer?.cancel();
//       }
//       setState(() {
//       });
//     });
//   }
//   void resetTimer() {
//     _timer?.cancel();
//     isTimeCounting = false;
//     secondsRemaining = TOTAL_SECONDS;
//   }


//   List get displayTime {
//     int minutes = (secondsRemaining ~/ 60);
//     int seconds = (secondsRemaining % 60);
//     String minutesStr = minutes.toString().padLeft(2, '0');
//     String secondsStr = seconds.toString().padLeft(2, '0');
//     // return '$minutesStr:$secondsStr';
//     return [minutesStr, secondsStr];
//   }


//   @override
//   Widget build(BuildContext context) {

//         return ColoredBox(
//           color: Colors.white,
//           child: SizedBox(
//               width: 80,
//               child: Row(
//                 children: [
//                   const Text("Timer :  "),
//                   Container(
//                     width: 23,
//                     height: 23,
//                     decoration: BoxDecoration(
//                       borderRadius: BorderRadius.circular(3),
//                       color: Colors.black,
//                     ),
//                     child: Center(
//                       child: Text(
//                  isTimeCounting == true? displayTime.first :"0",
//                         style: GoogleFonts.poppins(
//                           color: Colors.white,
//                           fontSize: 12,
//                           fontWeight: FontWeight.w500,
//                         ),
//                       ),
//                     ),
//                   ),
//                   5.horizontalSpace,
//                   Text(
//                     ":",
//                     style: GoogleFonts.poppins(
//                       color: Colors.black,
//                       fontSize: 12,
//                       fontWeight: FontWeight.w500,
//                     ),
//                   ),
//                   5.horizontalSpace,
//                   Container(
//                     width: 23,
//                     height: 23,
//                     decoration: BoxDecoration(
//                       borderRadius: BorderRadius.circular(3),
//                       color: Colors.black,
//                     ),
//                     child: Center(
//                       child: Text(
//                          isTimeCounting == true? displayTime.last :"0",
//                         style: GoogleFonts.poppins(
//                           color: Colors.white,
//                           fontSize: 12,
//                           fontWeight: FontWeight.w500,
//                         ),
//                       ),
//                     ),
//                   ),
//                 ],
//               ),
//             ),
          
//         );
//       }

//     }

