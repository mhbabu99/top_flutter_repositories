import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CustomWidgetConst {
  static customDropDownWidget(
      {title,
      List? data,
      bool initial = false,
      key,
      hintText,
      selectedData,
      insideBorderTitle = true,
      outsideBorderTitle = false,
      type = "others",
      svgIcon,

      //  SizingInformation? sizingInformation,
      context,
      colors}) {
    var brightness = MediaQuery.of(context).platformBrightness;
    // ignore: unused_local_variable
    bool isDarkMode = brightness == Brightness.dark;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          height: 65.5.h,
          decoration: BoxDecoration(
              color: colors ?? Colors.blue,
              borderRadius: BorderRadius.circular(10),
              border: Border.all(width: 1, color: Colors.black12)),
          alignment: Alignment.center,
          padding: EdgeInsets.only(left: 2.w),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              1.verticalSpace,
              Row(
                children: [
               
                  5.horizontalSpace,
                  Expanded(
                    flex: 15,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        5.verticalSpace,
                        Text(
                          title,
                          style:
                              TextStyle(color: Colors.black, fontSize: 10.sp,fontWeight: FontWeight.w100),
                        ),
                        DropdownButton<dynamic>(
                          iconSize: 25.sp,
                          icon: Padding(
                            padding: const EdgeInsets.only(right: 10),
                            child: Icon(
                              Icons.keyboard_arrow_down_sharp,
                              color: initial ? Colors.black : Colors.black,
                            ),
                          ),
                          hint: Padding(
                            padding: const EdgeInsets.only(left: 0),
                            child: Text(
                                    "$hintText",
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        fontSize: 14.sp, color: Colors.black),
                                  ),
                          ),
                          underline: const SizedBox(),
                          isExpanded: true,
                          borderRadius: BorderRadius.circular(10),
                          dropdownColor: Colors.white,
                          padding: EdgeInsets.only(right: 5.w),
                          isDense: true,
                          items: data!.map((value) {
                            return DropdownMenuItem<dynamic>(
                              value: value,
                              
                              child: Padding(
                                padding:
                                    const EdgeInsets.only(left: 0, right: 5),
                                child: Text(
                                  "${value[key]}",
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 12.sp),
                                ),
                              ),
                            );
                          }).toList(),
                          onChanged: (val) async {
                            selectedData(val);
                          },
                        ),
                        // !insideBorderTitle
                        //     ? const SizedBox(height: 10)
                        //     : const SizedBox(),
                        10.verticalSpace,
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        // Divider(
        //   height: .1.h,
        //   color: Colors.black,
        //   thickness: 1,
        // ),
      ],
    );
  }



}
