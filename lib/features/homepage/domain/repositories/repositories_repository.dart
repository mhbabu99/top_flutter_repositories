import 'package:top_flutter_repositories/features/homepage/domain/entities/repositories_entities.dart';

abstract class RepositoriesRepository {
  Future<RepositoriesEntities> getRepositories({required int page , required String name, required String short,required String star});
}