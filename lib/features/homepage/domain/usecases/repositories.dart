import 'package:top_flutter_repositories/features/homepage/domain/entities/repositories_entities.dart';
import 'package:top_flutter_repositories/features/homepage/domain/repositories/repositories_repository.dart';

class RepositoriesUseCase {
  final RepositoriesRepository repositoriesRepository ;

  RepositoriesUseCase({required this.repositoriesRepository});

  Future<RepositoriesEntities> getRepositories({required int page, required String name, required String short, required String star}) async {
    return await repositoriesRepository.getRepositories(page: page,name: name,short: short,star: star);
  }
}