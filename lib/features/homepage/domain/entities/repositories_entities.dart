import 'package:equatable/equatable.dart';
import 'package:top_flutter_repositories/features/homepage/data/models/repositories_model.dart';

// ignore: must_be_immutable
class RepositoriesEntities extends Equatable {
  final int totalCount;
  final bool incompleteResults;
   List<Item> items;

   RepositoriesEntities({
    required this.totalCount,
    required this.incompleteResults,
    required  this.items,
  });

  RepositoriesEntities copyWith({
    int? totalCount,
    bool? incompleteResults,
    List<Item>? items,
  }) {
    return RepositoriesEntities(
      totalCount: totalCount ?? this.totalCount,
      incompleteResults: incompleteResults ?? this.incompleteResults,
      items: items ?? this.items,
    );
  }

  @override
  String toString() {
    return 'TodoEntities{id: $totalCount, todo: $incompleteResults,}';
  }

  @override
  List<Object?> get props => [
        totalCount,
        incompleteResults,
        items,
      ];
}
