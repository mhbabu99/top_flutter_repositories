import 'dart:developer';

import 'package:top_flutter_repositories/core/network/dio_client/client.dart';
import 'package:top_flutter_repositories/core/network/dio_client/request_params.dart';
import 'package:top_flutter_repositories/features/homepage/data/models/repositories_model.dart';

class RepositoriesRemoteServices {
  final DioClient _dioClient = DioClient.instance;

  Future<RepositoriesModel> getRepositories({required int page, required String name, 
  required String short, required String star}) async {
    final APIRequestParam param = APIRequestParam(
      //xawa
      path: 'search/repositories?q=$name&page=$page&per_page=10',
      doCache: false,
    );
    return await _dioClient.get(param).then((response) {
      return response.fold((l) {
        log("Exception : $l");
        throw l;
      }, (r) {
        try {
          final RepositoriesModel repositoriess = RepositoriesModel.fromJson(r.data);
          return repositoriess;
        } on Exception catch (e) {
          throw e;
        }
      });
    });
  }
}
