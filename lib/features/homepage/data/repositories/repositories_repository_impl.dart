import 'package:top_flutter_repositories/features/homepage/data/datasources/repositories_remote.dart';
import 'package:top_flutter_repositories/features/homepage/domain/entities/repositories_entities.dart';
import 'package:top_flutter_repositories/features/homepage/domain/repositories/repositories_repository.dart';

class RepositoriesRepositoryImpl extends RepositoriesRepository {
  final RepositoriesRemoteServices repositoriesRemoteServices;

  RepositoriesRepositoryImpl({required this.repositoriesRemoteServices});

  @override
  Future<RepositoriesEntities> getRepositories({required int page,required String name, required String short,required String star}) async {


    return await repositoriesRemoteServices.getRepositories(page: page,name: name,short: short,star: star).then((response) {
      return response;
    }).onError((error, stackTrace) {
      throw error.toString();
    });

    
  }
}
