enum NormalApiState {
  loading,
  loaded,
  failure,
  initial,
  lazy,
}
